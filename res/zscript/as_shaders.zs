class ShaderHandler : EventHandler
{
	float glitchAmount;

	override void WorldLoaded(WorldEvent e)
	{
		glitchAmount = 0.0;
	}

	override void RenderOverlay(RenderEvent e)
	{
		PlayerInfo p = players[consoleplayer];

		Shader.SetEnabled(p,"grain",true);
		Shader.SetUniform1f(p,"grain","Timer",(gametic+e.fractic)/35.0);

		Shader.SetEnabled(p,"glitch",true);
		Shader.SetUniform1f(p,"glitch","Timer",(gametic+e.fractic)/35.0);
		Shader.SetUniform1f(p,"glitch","GlitchAmount",glitchAmount);
	}
}