class AS_Util
{
	// Math
	static float Sign(float n)
	{
		if (n >= 0.0) return 1.0;
		else if (n < 0.0) return -1.0;
		return 0.0;
	}

	static float SmoothTo(float c, float t, float a, float dt)
	{
		float sig = AS_Util.Sign(t-c);
		float nc = c+sig*a*dt;
		float sig2 = AS_Util.Sign(t-nc);
		if (sig2 != sig)
		{
			return t;
		}
		return nc;
	}


	// Actors
	static Actor GetActor(int tid, Class<Actor> type)
	{
		return level.CreateActorIterator(tid, type).Next();
	}
}