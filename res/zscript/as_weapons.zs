// base weapon class (originally from Soundless Mound)
class AS_Weapon : DoomWeapon abstract
{
	double speedfact, holdspeedfact;

	Property SpeedFactor: speedfact;
	Property HoldSpeedFactor: holdspeedfact;

	// generic function for "swing" mechanics, used by crowbar and pistol
	action void A_Swing( double x = 0.0, double y = 0.0, double recoil = 0.0 )
	{
		A_SetPitch(pitch+y,SPF_INTERPOLATE);
		A_SetAngle(angle+x,SPF_INTERPOLATE);
		A_Recoil(recoil);
	}
	override double GetSpeedFactor()
	{
		double factor = 1.0;
		if ( !Owner.player ) return factor;
		if ( Owner.player.ReadyWeapon == self )
		{
			if ( !(Owner.player.weaponstate&WF_WEAPONBOBBING) || (Owner.player.cmd.buttons&BT_ATTACK) )
				factor *= holdspeedfact;
			else
				factor *= speedfact;
		}
		return factor;
	}
	Default
	{
		Weapon.BobStyle "Alpha";
		Weapon.BobSpeed 1.5;
		Weapon.BobRangeX 1.5;
		AS_Weapon.SpeedFactor 1.0;
		AS_Weapon.HoldSpeedFactor 1.0;
		+INVENTORY.UNDROPPABLE;
		+INVENTORY.UNTOSSABLE;
		+WEAPON.NOALERT;
		+WEAPON.NO_AUTO_SWITCH;
	}
}

class AS_SparkLight : DynamicLight
{
	Default
	{
		DynamicLight.Type "Point";
	}

	override void PostBeginPlay()
	{
		Super.PostBeginPlay();
		args[LIGHT_INTENSITY] = 4;
	}

	override void Tick()
	{
		Super.Tick();
		if ( !target )
		{
			Destroy();
			return;
		}
		SetOrigin(target.pos,true);
		args[LIGHT_RED] = int(64*target.alpha);
		args[LIGHT_GREEN] = int(48*target.alpha);
		args[LIGHT_BLUE] = int(24*target.alpha);
	}
}

// sparks that fly from hitting walls
class AS_Spark : Actor
{
	Default
	{
		Radius 2;
		Height 4;
		+NOBLOCKMAP;
		+FORCEXYBILLBOARD;
		+MISSILE;
		+MOVEWITHSECTOR;
		+THRUACTORS;
		BounceType "Doom";
		BounceFactor 0.4;
		Gravity 0.2;
		Scale 0.2;
	}

	override void PostBeginPlay()
	{
		super.PostBeginPlay();
		let l = Spawn("AS_SparkLight",pos);
		l.target = self;
	}

	States
	{
	Spawn:
		SPRK A 1 bright A_FadeOut(0.01);
		wait;
	Death:
		SPRK A 1 bright A_FadeOut(0.05);
		wait;
	}
}

const FAXE_RANGE = 64.;

class AS_FireAxePuff : Actor
{
	Default
	{
		+NOBLOCKMAP;
		+NOGRAVITY;
		+NOEXTREMEDEATH;
		+PUFFONACTORS;
		SeeSound "axe/hit";
		AttackSound "axe/wall";
	}

	void A_Sparks()
	{
		// distance self from walls and planes
		Vector3 adjusted = pos;
		adjusted.xy += (cos(angle),sin(angle))*1;
		if ( adjusted.z > (ceilingz-1) ) adjusted.z = ceilingz-1;
		if ( adjusted.z < (floorz+1) ) adjusted.z = floorz+1;
		SetOrigin(adjusted,false);
		double ang, pt;
		for ( int i=0; i<8; i++ )
		{
			let s = Spawn("AS_Spark",pos);
			ang = random(0,360);
			pt = random(-90,90);
			s.vel = (cos(pt)*cos(ang),cos(pt)*sin(ang),-sin(pt))*random(2.0,4.0);
		}
	}

	States
	{
	Spawn:
		TNT1 A 1;
		Stop;
	Crash:
		TNT1 A 1 A_Sparks();
		Stop;
	}
}

class AS_FireAxeWeapon : AS_Weapon
{
	double charge;
	bool crit;

	Default
	{
		Tag "FireAxe";
		Inventory.PickupSound "axe/crit";
		AS_Weapon.SpeedFactor 0.75;
		AS_Weapon.HoldSpeedFactor 0.4;
		+WEAPON.MELEEWEAPON;
	}

	States
	{
	Spawn:
		FAXI A -1;
		stop;

	Ready:
		FAXE A 1 { invoker.charge = 1.; A_WeaponReady(); }
		wait;

	Fire:
		FAXE CB 3;
		FAXE B 10 A_ReFire;
		FAXE BC 2;
		FAXE DE 1;
		FAXE F 1 AxeAttack;
		FAXE F 10;
		FAXE ED 2;
		goto Ready;

	Hold:
		FAXE B 1;
		FAXE B 10 A_ReFire;
		goto Fire+4;

	Select:
		FAXE A 1 A_Raise;
		loop;

	Deselect:
		FAXE A 1 A_Lower;
		loop;
	}

	action void AxeAttack()
	{
		FTranslatedLineTarget t;
		int dmg = random(40, 80);
		double pt = AimLineAttack(angle,FAXE_RANGE,null,0.,ALF_CHECK3D);
		LineAttack(angle,FAXE_RANGE,pt,dmg,'Melee',"AS_FireAxePuff",LAF_ISMELEEATTACK,t);
		A_AlertMonsters(300);

		if (t.linetarget)
		{
			angle = t.angleFromSource;
			A_QuakeEx(2,2,2,8,0,30,"",QF_RELATIVE|QF_SCALEDOWN);
		}
		else
		{
			A_PlaySound("axe/swing", CHAN_WEAPON);
		}
	}
}

class AS_DummyWeapon : AS_Weapon
{
	Default
	{
		AS_Weapon.SpeedFactor 1;
		AS_Weapon.HoldSpeedFactor 1;
		+WEAPON.MELEEWEAPON;
	}

	States
	{
	Spawn:
		TNT1 A -1;
		stop;

	Ready:
		TNT1 A 1 A_WeaponReady;
		wait;

	Fire:
		TNT1 A 1;
		goto Ready;

	Select:
		TNT1 A 1 A_Raise(24);
		loop;

	Deselect:
		TNT1 A 1 A_Lower(24);
		loop;
	}
}