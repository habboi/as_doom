class AS_TeleSphere : SwitchingDecoration
{
	bool isActive;

	Default
	{
		Radius 12;
		Height 24;
		+NOGRAVITY
		+NOCLIP
	}
	
	States
	{
	Spawn:
		TELS A 1;
		goto Init;
		
	Init:
		TELS A 0
		{
			isActive = false;
			Thing_Deactivate(TID + TELEPORTER_TID_OFFSET);
		}
		TELS A -1;
		stop;
		
	Active:
		TELS A 0
		{
			isActive = true;
			Thing_Activate(TID + 1);
			Thing_Activate(TID + TELEPORTER_TID_OFFSET);
		}
		TELS A -1;
		stop;
	}
	
	static int CheckTSActive(Actor act)
	{
		AS_TeleSphere ts = AS_TeleSphere(act);
		if (ts.isActive) return 1;
		else return 0;
	}
}