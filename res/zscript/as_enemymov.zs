class AS_EnemyMov : Actor
{
	Default
	{
		Radius 20;
		Height 64;
		RenderStyle "Translucent";
		Alpha 1.0;
		Species "Mov";
		Health 120;
		DeathSound "MOVDEATH";
		+ISMONSTER
		+COUNTKILL
		+GHOST
		+NoInfighting
	}
	
	States
	{
	Spawn:
		MOVI A 1;
		goto Init;

	Init:
		MOVI A 0 MovInit;
		MOVI A 1 A_PlaySound("MOVDRONE", CHAN_VOICE, 0.1, true);
		goto Idle;

	Idle:
		MOVI A 1 A_Look;
		MOVI A 1 MovUpdate;
		loop;

	Idle1:
		MOVI B 1 A_Look;
		MOVI B 1 MovUpdate;
		loop;
		
	Idle2:
		MOVI C 1 A_Look;
		MOVI C 1 MovUpdate;
		loop;

	Idle3:
		MOVI D 1 A_Look;
		MOVI D 1 MovUpdate;
		loop;

	Idle4:
		MOVI E 1 A_Look;
		MOVI E 1 MovUpdate;
		loop;

	Idle5:
		MOVI F 1 A_Look;
		MOVI F 1 MovUpdate;
		loop;

	Idle6:
		MOVI G 1 A_Look;
		MOVI G 1 MovUpdate;
		loop;

	Idle7:
		MOVI H 1 A_Look;
		MOVI H 1 MovUpdate;
		loop;

	Idle8:
		MOVI I 1 A_Look;
		MOVI I 1 MovUpdate;
		loop;

	Attack:
		MOVI I 0 A_ChangeFlag("SHOOTABLE", true);
		MOVA A 5 A_FaceTarget;
		MOVA A 0 MovPreAttack;
		MOVA B 10 bright light("MOVATTACK") A_CustomRailgun(65, 0, "EDEDED", "White", RGF_FULLBRIGHT | RGF_SILENT);
		MOVA A 0 MovPostAttack;
		MOVI I 0 A_ChangeFlag("SHOOTABLE", false);
		MOVI A 1 MovUpdate;
		loop;

	Death:
		MOVI I 3 A_FadeTo(0.9, 0.1, false);
		MOVI H 3 A_FadeTo(0.8, 0.1, false);
		MOVI G 3 { A_Scream(); A_FadeTo(0.7, 0.1, false); }
		MOVI F 3 { A_NoBlocking(); A_FadeTo(0.7, 0.1, false); }
		MOVI E 3 A_FadeTo(0.5, 0.1, false);
		MOVI D 3 A_FadeTo(0.4, 0.1, false);
		MOVI C 3 A_FadeTo(0.3, 0.1, false);
		MOVI B 3 A_FadeTo(0.2, 0.1, false);
		MOVI A 3 A_FadeTo(0.1, 0.1, false);
		MOVI A 3 A_FadeTo(0.0, 0.1, true);
		stop;
	}

	float prevFactor;
	int lightTID;

	override void BeginPlay()
	{
		prevFactor = 0.0;
		//lightTID = FindUniqueTID();

		// creates a white light to use when attacking
		//A_SpawnItemEx("PointLight", 0, 0, 0,  0, 0, 0,  0, 0, 0,  lightTID);
		//let light = AS_Util.GetActor(lightTID, "PointLight");
		//light.args[DynamicLight.LIGHT_RED] = 255;
		//light.args[DynamicLight.LIGHT_GREEN] = 255;
		//light.args[DynamicLight.LIGHT_BLUE] = 255;
		//light.args[DynamicLight.LIGHT_INTENSITY] = 75;
	}

	action void MovInit()
	{
		//Thing_Deactivate(invoker.lightTID);
	}

	action void MovPreAttack()
	{
		A_PlaySound("MOVSHOT", CHAN_WEAPON, 1.0);
		//Thing_Activate(invoker.lightTID);
	}

	action void MovPostAttack()
	{
		//Thing_Deactivate(invoker.lightTID);
	}
	
	action void MovUpdate()
	{
		if (CheckProximity("AS_Player", 512.0, 1, CPXF_CHECKSIGHT))
		{
			let player = AS_Util.GetActor(PLAYER_TID, "AS_Player");
			let vel = player.vel;
			let sqrSpeed = (vel.x*vel.x) + (vel.y*vel.y) + (vel.z*vel.z);
			let maxSpeed = 5.0;
			let curFactor = min(sqrSpeed / (maxSpeed*maxSpeed), 1.0);
			let factor = AS_Util.SmoothTo(invoker.prevFactor, curFactor, 0.9, 2.0/35.0);
			invoker.prevFactor = factor;
		}
		else
		{
			invoker.prevFactor = AS_Util.SmoothTo(invoker.prevFactor, 0.0, 0.9, 2.0/35.0);
		}

		let f = invoker.prevFactor;
		A_SoundVolume(CHAN_VOICE, max(f, 0.1));

		if (f < 0.11)
		{
			SetStateLabel("Idle");
		}
		else if (f < 0.22)
		{
			SetStateLabel("Idle1");
		}
		else if (f < 0.33)
		{
			SetStateLabel("Idle2");
		}
		else if (f < 0.44)
		{
			SetStateLabel("Idle3");
		}
		else if (f < 0.55)
		{
			SetStateLabel("Idle4");
		}
		else if (f < 0.66)
		{
			SetStateLabel("Idle5");
		}
		else if (f < 0.77)
		{
			SetStateLabel("Idle6");
		}
		else if (f < 0.88)
		{
			SetStateLabel("Idle7");
		}
		else if (f < 1.0)
		{
			SetStateLabel("Idle8");
		}
		else
		{
			SetStateLabel("Attack");
		}
	}
}