// Player class

class AS_Player : DoomPlayer
{
	Default
	{
		Player.WeaponSlot 1, "AS_DummyWeapon", "AS_FireAxeWeapon";
		Player.WeaponSlot 2, "Pistol";
		Player.StartItem "AS_DummyWeapon";
	}

	override void BeginPlay()
	{
		super.BeginPlay();
		//ClearInventory();
		GiveInventory("AS_DummyWeapon", 1);
	}

	override void Tick()
	{
		super.Tick();

		float maxGlitch = 0;
		ThinkerIterator ashFinder = ThinkerIterator.Create("AS_EnemyAshloth");
		AS_EnemyAshloth ash;
		while (ash = AS_EnemyAshloth(ashFinder.Next()))
		{
			let dif = ash.pos - self.pos;
			let sqrdist = (dif.x*dif.x) + (dif.y*dif.y) + (dif.z*dif.z);
			let maxdist = ASHLOTH_LIGHT_RADIUS*2;
			let gfactor = 1.0 - clamp(sqrdist / (maxdist*maxdist), 0.0, 1.0);
			gfactor *= ash.LightDamageValue();
			if (gfactor > maxGlitch)
			{
				maxGlitch = gfactor;
			}
		}

		ShaderHandler sh = ShaderHandler(EventHandler.Find("ShaderHandler"));
		sh.glitchAmount = maxGlitch;
	}
}