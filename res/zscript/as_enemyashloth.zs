class AS_AshlothLight : PointLight
{
	Default
	{
		+DYNAMICLIGHT.SUBTRACTIVE
	}
}

class AS_EnemyAshloth : Arachnotron
{
	Default
	{
		RenderStyle "Stencil";
		StencilColor "Black";
		Alpha 0.8;
		Speed 24;
		PainChance 256;
		Health 700;
		SeeSound "ash/awake";
		-SOLID
	}

	States
	{
	Spawn:
		BSPI AB 10 A_FadeTo(0.8, 1.0, false);
		goto Idle;

	Idle:
		BSPI A 0 AshInit;
		BSPI AB 10 A_Look;
		goto Idle+1;

	See:
		BSPI A 0 A_FadeTo(0.0, 1.0, false);
		BSPI A 0 AshAwake;
		BSPI A 20;
		BSPI A 0 AshAwake;
    	BSPI A 3 AshSounds;
    	BSPI ABBCC 3 A_Chase;
    	BSPI D 3 AshSounds;
    	BSPI DEEFF 3 A_Chase;
		goto See+3;

	Missile:
		BSPI A 1 A_FadeTo(0.0, 1.0, false);
		BSPI A 10 A_FaceTarget;
		BSPI A 0 A_SetSpeed(42);
		BSPI A 3 AshSounds;
		BSPI ABBCC 3 A_Chase("Meele", "Missile+6", CHF_NORANDOMTURN);
		BSPI D 3 AshSounds;
		BSPI DEEFF 3 A_Chase("Meele", "Missile+12", CHF_NORANDOMTURN);
		BSPI A 0 A_SetSpeed(24);
		BSPI A 0 A_FadeTo(0.0, 1.0, false);
		goto See+3;

	Pain:
		BSPI I 3 A_FadeTo(0.8, 1.0, false);
		BSPI I 1 A_Jump(64, 4);
		BSPI I 0 AshPrePain;
    	BSPI I 6 A_Pain;
    	BSPI HI 6;
    	BSPI I 0 AshPostPain;
    	BSPI I 3 A_FadeTo(0.0, 1.0, false);
    	goto See+3;

    Death:
    	BSPI A 0 AshDeath;
    	goto super::Death;
	}

	AS_AshlothLight plight;
	int dmgTicks;
	int growTicks;

	float LightDamageValue()
	{
		if (plight)
			return float(plight.args[3]) / float(ASHLOTH_LIGHT_RADIUS);
		
		return 0.0;
	}

	action void AshInit()
	{
		let light = AS_AshlothLight(Spawn("AS_AshlothLight", pos));
		light.args[0] = 255; light.args[1] = 255; light.args[2] = 255;
		light.args[3] = 0;
		invoker.plight = light;

		invoker.growTicks = -1;
		invoker.dmgTicks = 0;
	}

	action void AshSounds()
	{
		A_BabyMetal();
		if (random(0.0, 1.0) < 0.1)
			A_PlaySound("ash/grunt", CHAN_6);
	}

	action void AshAwake()
	{
		//invoker.growTicks = 35;
		//invoker.plight.args[3] = ASHLOTH_LIGHT_RADIUS;
	}

	action void AshPrePain()
	{
		invoker.growTicks = -1;
		//invoker.plight.args[3] = 0;
	}

	action void AshPostPain()
	{
		invoker.growTicks = 35;
		//invoker.plight.args[3] = ASHLOTH_LIGHT_RADIUS;
	}

	action void AshDeath()
	{
		invoker.plight.args[3] = 0;
		A_FadeTo(0.8, 1.0, false);
	}

	override void Tick()
	{
		super.Tick();

		if (plight)
		{
			plight.SetOrigin(self.pos, false);

			if (growTicks > 0)
			{
				plight.args[3] = ASHLOTH_LIGHT_RADIUS * (1.0 - (growTicks / 35.0));
				growTicks -= 1;
			}
			else if (growTicks == -1)
			{
				plight.args[3] = 0;
			}
		}

		if (target && plight.args[3] > 0 && dmgTicks >= 35)
		{
			dmgTicks = 0;
			A_Explode(40, plight.args[3]*2, XF_NOTMISSILE);
		}
		dmgTicks += 1;
	}
}