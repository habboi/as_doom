// Originally by Daedelus in https://www.shadertoy.com/view/XltGRf

// IQ's noise value 2D
float hash( vec2 p )
{
	float h = dot(p + Timer * 0.0001,vec2(127.1,311.7));
    return fract(sin(h)*43758.5453123);
}
float hash2( vec2 p )
{
	float h = dot(p ,vec2(127.1,311.7));
    return fract(sin(h)*43758.5453123);
}

float noise( in vec2 p )
{
    vec2 i = floor( p );
    vec2 f = fract( p );
    	
	vec2 u = f*f*(3.0-2.0*f);

    return mix( mix( hash( i + vec2(0.0,0.0) ), 
                     hash( i + vec2(1.0,0.0) ), u.x),
                mix( hash( i + vec2(0.0,1.0) ), 
                     hash( i + vec2(1.0,1.0) ), u.x), u.y);
}


void main()
{
    float iTime = Timer;
    vec2 iResolution = textureSize(InputTexture, 0);
    vec2 uv = TexCoord;
    vec4 res = texture(InputTexture, uv);
    
    float glitch = hash2(floor(iTime * 4.0 - uv * 20.0) / 20.0);
    uv += pow(fract(glitch + iTime * 0.1), 30.0);
    
    float n = pow((noise(uv * 150.0) + noise(uv * 350.0)) * 0.5, 3.0);
    n *= abs(sin(uv.y * 250.0 + iTime * 2.0));
    float offset = sign(sin(uv.x * 6.0 + iTime * 3.0)) * 0.1;
    offset += sign(sin(uv.y * 10.0 + uv.x * 14.5 + iTime * 20.0)) * 0.1;
    offset += sign(sin(sin(iTime) * uv.y * 12.0 - uv.x * 22.0 - iTime * 40.0)) * 0.2;
    offset = pow(offset - 0.5, 4.0);
        
    n *= min(1.0, floor((1.5 - uv.y + offset) * 3.0) / 3.0);
    
    float waver = abs(noise(vec2(iTime * 3.7)) * 2.0);
    n *= pow(fract(floor(uv.y * 15.0) / 15.0 + iTime * 0.3) * waver * 0.4 + 0.4, 0.65);
    n *= 0.75 + waver * 0.125;
    n = clamp(n, 0.0, 1.0);
    
	FragColor = mix(res, vec4(n), GlitchAmount);
}