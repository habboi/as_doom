# TODO

- Change Ashloth sounds

# Enemies

## MOV (as_enemymov)

Former prisioner whose soul became so corrupted by darkness that all that's left is the static ectoplasm of it's existence. They can't see or hear anything from the material world but detects energy disturbances of living souls in it's surrounding area.

- Attack: Detects player movement and when it's fully triggered, fires a deadly Railgun attack.
- Damage: 45 to 70
- Killable: Only with Plasma Gun
- Can damage others: Yes


## Ashloth (as_enemyashloth)

A spider-like creature which doesn't live in this plane of existence but affects the perception of reality of those who crosses it's path.

- Damage: 15 per second in contact
- Killable: Yes
- Can damage others: Yes


## Demon Statue (as_enemystatue)

A petrified demon which can only move when there are no living eyes looking at it.

- Damage: 35 to 45
- Killable: Only with Rocket Launcher or by Ghost
- Can damage others: No



# Credits

## Sound (freesound.org)

- Willyzwb4twan
- complex_waveform